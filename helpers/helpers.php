<?php

if (! function_exists('pushkin')) {
    function pushkin($input)
    {
        return (new \Pushkin\Plugin\Services\Reader())->translate($input);
    }
}
