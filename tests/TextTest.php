<?php

class TextTest extends \PHPUnit\Framework\TestCase
{
    protected $faker;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @test
     */
    public function text_can_be_traced_to_the_original_file()
    {
        $text = new \Pushkin\Plugin\Text("Test " .
            23);

        $this->assertEquals('Test 23', $text->rendered);
        $this->assertEquals('"Test " . 23', $text->source);
    }
}