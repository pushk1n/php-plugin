<?php

class HelperFunctionTest extends \PHPUnit\Framework\TestCase
{
    protected $faker;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @test
     */
    public function helper_function_returns_new_string()
    {
        $text = $this->faker->sentence;
        $this->assertEquals($text, pushkin($text));
    }
}