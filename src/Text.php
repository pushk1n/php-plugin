<?php

namespace Pushkin\Plugin;

use PhpParser\Error;
use PhpParser\NodeDumper;
use PhpParser\ParserFactory;

class Text {
    public $source;

    public $rendered;

    public $replacement;

    public function __construct($rendered)
    {
        $this->rendered = $rendered;
        $this->replacement = $rendered;

        $this->findSource();
    }

    protected function findSource()
    {
        $currentFolder = dirname(__FILE__);

        $stack = array_filter(debug_backtrace(), function($call) use ($currentFolder) {
            return isset($call['file']) && strpos($call['file'], $currentFolder) !== 0;
        });

        $lastCall = array_shift($stack);

        $this->source = $this->readLine($lastCall['file'], $lastCall['line']);

        $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        try {
            $ast = $parser->parse("<?php\n".$this->source);
        } catch (Error $error) {
            echo "Parse error: {$error->getMessage()}\n";
            return;
        }

        $dumper = new NodeDumper();
        echo $dumper->dump($ast) . "\n"; die();
    }

    /**
     * @param $file
     * @param $lineNo
     * @return string
     */
    protected function readLine($file, $lineNo)
    {
        $lines = file($file);

        return $lines[$lineNo - 1];
    }
}