<?php

namespace Pushkin\Plugin\Services;

use Pushkin\Plugin\Text;

class Reader {
    /**
     * @param $input
     * @return string
     */
    public function translate($input) {
        return (new Text($input))->replacement;
    }
}